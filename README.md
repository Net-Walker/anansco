# anasco

I'm using redux /react-redux because it was recommended in the test guideline.
Based on the requirements and scope I would not suggest redux until the need for it arises.
a simple fetch would do the job effectively.

I'm also be using typescript to enforce type (strict rules). This can also be achieved using @jsdoc.

## Improvements

### Pagination

modify the json output->api to accept rows count.
Base on the feature above we would implement inifinte scrolling for the list of offers.

### Filters/Sorts

A good and mandatory feature to have would be to filter and sort products by prices etc

### Expand Offer view

User should be able to view more information about the offer. A right sliding dialogue that slides from the right of screen would solve this, inorder to display more information about the offer.

## Run Application

use `yarn start` to run application in development mode.
