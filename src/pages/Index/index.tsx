import './style.scss';

import Footer from '../../components/Footer/Footer';
import Header from '../../components/Header/Header';
import Offers from '../../components/Offers/Offers';

const Index = () => {  
  return (
    <>
      <Header />
      <Offers />
      <Footer />
    </>
  );
}

export default Index