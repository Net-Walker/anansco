import './app.scss'

import { applyMiddleware, createStore } from "redux";

import Index from './pages/Index'
import {Provider} from 'react-redux';
import reducer from "./store/store";
import thunk from "redux-thunk";

const store = createStore(reducer, applyMiddleware(thunk));

const App = () => {
  return (
      <div className='app'>
        <Provider store={store}>
            <Index />
        </Provider>
      </div>
  );
}

export default App;