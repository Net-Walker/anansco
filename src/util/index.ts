export const truncate = (input: string, pos: number) => {
    if (input.length > pos) {
      return input.substring(0, pos) + '...';
    }
   return input;
}