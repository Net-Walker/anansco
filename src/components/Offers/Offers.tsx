import './style.scss';

import { IOffers, IOffersRes } from '../../store/types';
import Offer, { IOffer } from '../Offer/Offer';
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from 'react';

import { Dispatch } from 'redux';
import client from '../../api/client';

const Offers = () => {
  const [loading, setLoading] = useState(false)
  const [error, setError] = useState({})
  const offSelector = useSelector((state: IOffersRes) => state);
  const dispatch = useDispatch();
  
  const getOffers = () => {
    setLoading(true)
    return (dispatch: Dispatch<any>) => {
      client('codingtask/offers.json').then((res: IOffers) => { // errors payload should have a uniform type in backend
        if (res.offers) {
          dispatch({
            type: "FETCH_DATA",
            data: res.offers
          }) 
        } else {
          // create error type and classify error base on status code log this to standard output ot pust log to eg. kibana
          setError(res)
        }
        setLoading(false)}
      );
    };
  }

  useEffect(() => {
    dispatch(getOffers());
  }, [dispatch]);
  
    return (
        <div className='offer-view'>
        <p className='loader'>
            {loading && 'loading offers...'}
        </p>
        {offSelector.data && <div className='offers'>
          {offSelector.data.map((offer: IOffer, index: number) => {
              return <Offer key={index} data={offer}/>
          })}
        </div>}
        {error && <div className="error-view">
            <p>Could not load offers at the moment. kindly contact administrator</p>
        </div>}
        </div>
  );
}

export default Offers