import './style.scss'

import fallbackImg from './asset/emptyImg.png'
import { truncate } from '../../util'

interface OfferProps {
    data: IOffer
}

export interface IOffer {
    description: string
    splashImages: image[]
    prices: prices
}

type image = {
    narrowLarge: string
    narrowMedium: string
    url	: string
}

type amount = {
    currency: string
    value: string
}
type dayPrice = {
    amount : amount
}

type prices = {
    dayPrice: dayPrice
}

const Offer = (props: OfferProps) => {
    const data = props.data
    let img = fallbackImg
    if (data.splashImages && data.splashImages[0]) {
        img = data.splashImages[0].narrowMedium
    }

    return <div className='offer'>
        <img src={img} alt="carImage" className='car-img' />
        <p title={data.description} className='offer-title'>{truncate(data.description, 30)}</p>
        <p className='price-block'><span className='offer-price-figure'>{data.prices.dayPrice.amount.value}</span> <span className='offer-price'>{data.prices.dayPrice.amount.currency}/day</span></p>
        </div>
}
export default Offer