import './style.scss'

const Footer = () => {
    return <footer>
                <p className='copyright'>© sixt-testing {new Date().getFullYear()}</p>
           </footer> 
}

export default Footer