import config from './../config/app.json'

const client: any = async (url: string): Promise<Response> => {
const _url = config.baseUrl + url
const response = await fetch(_url)
    if (!response.ok) {
        console.error(config.logPrefix + ': unable to make request to ' + _url + ' ' + Error(response.statusText))
        return response
        // log error to standard output && send logs to kibana.
    }
    const data = await (response.json() as Promise<Response>)
    return data
}

export default client
// This should be configure to allow other methods not just GET.

